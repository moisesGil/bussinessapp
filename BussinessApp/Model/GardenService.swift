//
//  GardenService.swift
//  BussinessApp
//
//  Created by Moises on 4/26/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class GardenService: NSObject {

    var title = ""
    var previewImage = UIImage()
    var serviceDescription = ""
    
    init(title: String , previewImage: UIImage, serviceDescription : String ){
        
        self.title = title
        self.previewImage = previewImage
        self.serviceDescription = serviceDescription
    }
    
    
}
