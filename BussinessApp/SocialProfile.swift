//
//  SocialProfile.swift
//  BussinessApp
//
//  Created by Moises on 5/9/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class SocialProfile: NSObject {
    
    
    var title = ""
    var message  = ""
    var image = UIImage()
    var url: URL!

}
