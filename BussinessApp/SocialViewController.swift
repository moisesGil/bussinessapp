//
//  SocialViewController.swift
//  BussinessApp
//
//  Created by Moises on 5/9/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class SocialViewController: UIViewController, UIWebViewDelegate {

    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewTitle = ""
    var socialProfileUrl:URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        webView.delegate = self

        // Do any additional setup after loading the view.
    
        //Setting the title of the view
        self.navigationItem.title = viewTitle
    
        //Creating request and showing page in the Web View
        let socialUrlRequest = URLRequest(url: socialProfileUrl)
        webView.loadRequest(socialUrlRequest)
        
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        activityIndicator.startAnimating()
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.stopAnimating()
    }

}
