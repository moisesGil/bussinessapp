//
//  PortafolioDatasource.swift
//  BussinessApp
//
//  Created by Moises Gil on 5/3/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class PortafolioDatasource: NSObject {
    
    
    let portafolioImages =  ["Portfolio1.jpg","Portfolio2.jpg","Portfolio3.jpg","Portfolio4.jpg","Portfolio5.jpg","Portfolio6.jpg","Portfolio1.jpg","Portfolio2.jpg","Portfolio3.jpg","Portfolio4.jpg","Portfolio5.jpg","Portfolio6.jpg","Portfolio1.jpg","Portfolio2.jpg","Portfolio3.jpg","Portfolio4.jpg","Portfolio5.jpg","Portfolio6.jpg"]
    

    func getData() -> [GardenPortafolio] {
        
        var portafolio = [GardenPortafolio]()
        
        for imageName in portafolioImages  {
            
            let gardenPortafolio = GardenPortafolio()
        
            gardenPortafolio.portafolioImage = UIImage(named: imageName)!
            
            portafolio.append(gardenPortafolio)
            
        }
        
        return portafolio
        
    }
    

}
