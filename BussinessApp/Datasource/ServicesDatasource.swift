//
//  ServicesDatasource.swift
//  BussinessApp
//
//  Created by Moises on 4/26/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class ServicesDatasource: NSObject {
    
    var imageList = ["Lawn.jpg","Pond.jpg","Hedge.jpg","Flowers.jpg","Lawn.jpg","Pond.jpg","Hedge.jpg","Flowers.jpg","Lawn.jpg","Pond.jpg","Hedge.jpg","Flowers.jpg"]
    
    var titleList = ["Lawn Mowing","Pond Cleaning","Hedge Trimming","Flower Planting","Lawn Mowing","Pond Cleaning","Hedge Trimming","Flower Planting","Lawn Mowing","Pond Cleaning","Hedge Trimming","Flower Planting"]
    
    
    func getServices()  -> [GardenService] {
        
        var services = [GardenService]()
        
        for (index , imageName) in imageList.enumerated() {
            
            let service = GardenService(title: titleList[index] , previewImage: UIImage(named: imageName)! , serviceDescription : "Here is the description" )
            
            services.append(service)
            
        }
        
        return services
    }
    
    

}
