//
//  SocialProfileDataProvider.swift
//  BussinessApp
//
//  Created by Moises on 5/9/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class SocialProfileDataProvider: NSObject {
    
    
    func getData() -> [SocialProfile] {
        
        
        let facebookProfile  = SocialProfile()
        
        facebookProfile.title = "Facebook"
        facebookProfile.message = "Like us on Facebook"
        facebookProfile.image = UIImage(named: "SocialIcon1.png")!
        facebookProfile.url = URL(string: "https://www.facebook.com")
        
        let twitterProfile = SocialProfile()
        
        twitterProfile.title = "Twitter"
        twitterProfile.message = "Follow us on Twitter"
        twitterProfile.image = UIImage(named: "SocialIcon2.png")!
        twitterProfile.url = URL(string: "https://www.twitter.com")
        
        
        let gPlusProfile = SocialProfile()
        
        gPlusProfile.title = "Google Plus"
        gPlusProfile.message = "Add us on Google Plus"
        gPlusProfile.image = UIImage(named: "SocialIcon3.png")!
        gPlusProfile.url = URL(string: "https://plus.google.com")
        
        
        let linkedInProfile = SocialProfile()
        
        linkedInProfile.title = "LinkedIn"
        linkedInProfile.message = "Connect to us to LinkedIn"
        linkedInProfile.image = UIImage(named: "SocialIcon4.png")!
        linkedInProfile.url = URL(string : "https://www.linkedin.com")
        
        
        let youtubeProfile = SocialProfile()
        
        youtubeProfile.title = "Title"
        youtubeProfile.message = "Subscribe to our Youtube Channel"
        youtubeProfile.image = UIImage(named: "SocialIcon5.png")!
        youtubeProfile.url = URL(string: "https://www.youtube.com")
        
        
        let ourWebPage = SocialProfile()
        
        ourWebPage.title = "Our Web site"
        ourWebPage.message = "Our Web Site"
        ourWebPage.image = UIImage(named: "SocialIcon6.png")!
        ourWebPage.url  = URL(string: "https://www.google.com")
        
        
        
        let socialProfiles = [facebookProfile,twitterProfile,gPlusProfile,linkedInProfile,youtubeProfile,ourWebPage]
    
        return socialProfiles
        
        
        
    }

}
