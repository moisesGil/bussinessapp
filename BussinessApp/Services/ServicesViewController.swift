//
//  ServicesViewController.swift
//  BussinessApp
//
//  Created by Moises on 4/26/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class ServicesViewController: UIViewController {
    
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //Garden Service Data
    var gardenServiceData: GardenService!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        serviceImageView.image = gardenServiceData.previewImage
        serviceTitleLabel.text = gardenServiceData.title
        descriptionTextView.text = gardenServiceData.serviceDescription
        
        serviceTitleLabel.textColor = UIColor(red: 76/255, green: 147/255, blue: 91/255, alpha: 1.0)
        
        //Replacing the text of the back button for improve the style
        
//        self.navigationItem.backBarButtonItem  = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        

        
        //This put the name of the current service at top for better looking
        self.navigationItem.title = gardenServiceData.title
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
