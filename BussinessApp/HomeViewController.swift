//
//  HomeViewController.swift
//  BussinessApp
//
//  Created by Moises on 4/26/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func goToPortafolio(_ sender: UIButton) {
        
        self.tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func goToService(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func goToContact(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 3
    }
    
    
    @IBAction func goToAboutUs(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 4
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
