//
//  PortafolioCollectionViewController.swift
//  BussinessApp
//
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

private let reuseIdentifier = "cell"

class PortafolioCollectionViewController: UICollectionViewController {
    
    var portafolio: [GardenPortafolio]!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Do any additional setup after loading the view.
        
        let portafolioDS = PortafolioDatasource()
         
        portafolio = portafolioDS.getData()
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return portafolio.count
    }


    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PortafolioCollectionViewCell
    
        // Configure the cell
        cell.portafolioImageView.image  = portafolio[indexPath.row].portafolioImage
        
        
        cell.layer.borderWidth = 6
        cell.layer.borderColor = UIColor(red: 255/255, green: 255/255 , blue: 255/255 ,alpha : 0.50).cgColor
        
        return cell
    }


}

