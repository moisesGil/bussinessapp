//
//  PortafolioCollectionViewCell.swift
//  BussinessApp
//
//  Created by Moises Gil on 5/3/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class PortafolioCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var portafolioImageView: UIImageView!
}
