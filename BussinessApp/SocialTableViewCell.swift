//
//  SocialTableViewCell.swift
//  BussinessApp
//
//  Created by Moises on 5/9/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit

class SocialTableViewCell: UITableViewCell {

    @IBOutlet weak var cellIconImageView: UIImageView!
    @IBOutlet weak var cellMessageLabel: UILabel!
    
    var cellData: SocialProfile!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
