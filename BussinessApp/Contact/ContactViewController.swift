//
//  ContactViewController.swift
//  BussinessApp
//
//  Created by Moises on 5/8/18.
//  Copyright © 2018 MeSoft. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var usLocationMapView: MKMapView!

    var latitude = 51.501364
    var longitude = -0.1418899999999894
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        //Configuring the map
        
        //Level of zoom of the map
        let span = MKCoordinateSpanMake(0.005, 0.005)
        
        //Map Location Region
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), span: span)
        usLocationMapView.setRegion(region,animated: true)
        
        
        //Pining in the map
        
        
        let pinObject = MKPointAnnotation()
    
         pinObject.coordinate =  CLLocationCoordinate2DMake(latitude, longitude) //Coordinates
         pinObject.title = "Gardern,Inc."  //Title of the ping
         pinObject.subtitle = "123 Street, London, United Kingdom" //Subtitle of the ping
        
        usLocationMapView.addAnnotation(pinObject)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        
    }
    
    @IBAction func showDirections(_ sender: UIButton) {
    
        UIApplication.shared.open(URL(string: "http://maps.apple.com/map?daddr=\(latitude),\(longitude)")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func doCall(_ sender: Any) {
        
        UIApplication.shared.open(URL(string: "tel://8098819090")!, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func sendEmail(_ sender: UIButton) {
        
        let recipients = ["garden@mail.com"]
        
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Email To Garden,Inc.")
        mailComposer.navigationBar.tintColor = UIColor.white
        mailComposer.setToRecipients(recipients)
        
        
        if mailComposer != nil {
         
            self.present(mailComposer, animated: true, completion: nil)

        }else {
            
            print("Something happened with the Mail Composer")
        
        }
        
        
    }
    
    
    //Dismissing the Mail Composer View
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
